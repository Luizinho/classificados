import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

//Plugin browser
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, private iab: InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  abreSite(url){

    let imoveis  = "http://ondeircidades.com.br/sistemas/#/ondeir/classifieds/estates/"
    let carros   = "http://ondeircidades.com.br/sistemas/#/ondeir/classifieds/cars/"
    let site: any;

    if(url == "imoveis"){
      site = imoveis;
    } else {
      site = carros;
    }

    const browser = this.iab.create(site, '_blank', {
      location : 'yes',
      clearcache : 'yes',
      zoom : 'no',
      enableViewportScale : 'no',
      closebuttoncaption : 'Fechar',
      hidenavigationbuttons: 'no',
      hideurlbar: 'no'
    });

  }

}
